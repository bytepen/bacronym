#!/bin/bash

set -e

main() {
    setup
    build
    project_info
}

setup() {
    	BASE_DIR="$(dirname $(cd "$(dirname $( cd "$(dirname "$0")" ; pwd -P ))"; pwd -P))"

	pushd "${BASE_DIR}"
		yarn install
	popd
}

build() {
	pushd "${BASE_DIR}"
		yarn lint
		yarn build
	popd
}

project_info() {
    touch "dist/project-info/$(date '+%s').push"
}

main
