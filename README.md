# Bacronym

Bacronym is a simple game that generates an acronym and gives you 60 seconds to come up with a defintion. This game is best played in a party setting where turns are taken with one person choosing the best acronym from the ones the rest of the group has created.
